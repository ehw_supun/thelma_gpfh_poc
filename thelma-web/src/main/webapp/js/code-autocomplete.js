var serviceCodes = {};
$(document).find('.service-code-autocomplete').typeahead({
	hint : true,
	highlight : true,
	minLength : 2,
	limit : 10
}, {
	source : function(q, cb) {
		return $.ajax({
			dataType : 'json',
			type : 'get',
			url : contextPath + 'autocomplete/getMbsCodes?code=' + q + '&type=' + $('#service-type').find(":selected").val(),
			chache : false,
			success : function(data) {
				var result = [];
				$.each(data, function(index, val) {
					key = val.itemNumber + ' - ' +  val.serviceDescription;
					serviceCodes[key] = val;
					result.push({
						value : key
					});
				});
				cb(result);
			}
		});
	}
});

$('.service-code-autocomplete').on('typeahead:selected', function(event, selection) {
	var selectedServiceItem = serviceCodes[selection.value];
	thelmaGpfhPatient.setServiceInfo(selectedServiceItem);
});