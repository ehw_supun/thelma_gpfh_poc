jQuery(document)
		.ready(
				function($) {

					$('.datepicker').datepicker({
						format : 'dd/mm/yyyy'
					});
					
					$('.datepicker').on('changeDate', function(ev){
					    $(this).datepicker('hide');
					});
					
					// Back to top button
					$(window).scroll(function() {
						if ($(this).scrollTop() > 100) {
							$('.back-to-top').fadeIn('slow');
						} else {
							$('.back-to-top').fadeOut('slow');
						}
					});
					$('.back-to-top').click(function() {
						$('html, body').animate({
							scrollTop : 0
						}, 1500, 'easeInOutExpo');
						return false;
					});

					// Stick the header at top on scroll
					$("#header").sticky({
						topSpacing : 0,
						zIndex : '50'
					});

					// Intro background carousel
					$("#intro-carousel").owlCarousel({
						autoplay : true,
						dots : false,
						loop : true,
						animateOut : 'fadeOut',
						items : 1
					});

					// Initiate the wowjs animation library
					new WOW().init();

					// Initiate superfish on nav menu
					$('.nav-menu').superfish({
						animation : {
							opacity : 'show'
						},
						speed : 400
					});

					// Mobile Navigation
					if ($('#nav-menu-container').length) {
						var $mobile_nav = $('#nav-menu-container').clone()
								.prop({
									id : 'mobile-nav'
								});
						$mobile_nav.find('> ul').attr({
							'class' : '',
							'id' : ''
						});
						$('body').append($mobile_nav);
						$('body')
								.prepend(
										'<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
						$('body').append('<div id="mobile-body-overly"></div>');
						$('#mobile-nav').find('.menu-has-children').prepend(
								'<i class="fa fa-chevron-down"></i>');

						$(document).on(
								'click',
								'.menu-has-children i',
								function(e) {
									$(this).next().toggleClass(
											'menu-item-active');
									$(this).nextAll('ul').eq(0).slideToggle();
									$(this).toggleClass(
											"fa-chevron-up fa-chevron-down");
								});

						$(document).on(
								'click',
								'#mobile-nav-toggle',
								function(e) {
									$('body').toggleClass('mobile-nav-active');
									$('#mobile-nav-toggle i').toggleClass(
											'fa-times fa-bars');
									$('#mobile-body-overly').toggle();
								});

						$(document)
								.click(
										function(e) {
											var container = $("#mobile-nav, #mobile-nav-toggle");
											if (!container.is(e.target)
													&& container.has(e.target).length === 0) {
												if ($('body').hasClass(
														'mobile-nav-active')) {
													$('body')
															.removeClass(
																	'mobile-nav-active');
													$('#mobile-nav-toggle i')
															.toggleClass(
																	'fa-times fa-bars');
													$('#mobile-body-overly')
															.fadeOut();
												}
											}
										});
					} else if ($("#mobile-nav, #mobile-nav-toggle").length) {
						$("#mobile-nav, #mobile-nav-toggle").hide();
					}

					// Smooth scroll for the menu and links with .scrollto
					// classes
					$('.nav-menu a, #mobile-nav a, .scrollto')
							.on(
									'click',
									function() {
										if (location.pathname
												.replace(/^\//, '') == this.pathname
												.replace(/^\//, '')
												&& location.hostname == this.hostname) {
											var target = $(this.hash);
											if (target.length) {
												var top_space = 0;

												if ($('#header').length) {
													top_space = $('#header')
															.outerHeight();

													if (!$('#header').hasClass(
															'header-fixed')) {
														top_space = top_space - 20;
													}
												}

												$('html, body')
														.animate(
																{
																	scrollTop : target
																			.offset().top
																			- top_space
																}, 1500,
																'easeInOutExpo');

												if ($(this)
														.parents('.nav-menu').length) {
													$('.nav-menu .menu-active')
															.removeClass(
																	'menu-active');
													$(this)
															.closest('li')
															.addClass(
																	'menu-active');
												}

												if ($('body').hasClass(
														'mobile-nav-active')) {
													$('body')
															.removeClass(
																	'mobile-nav-active');
													$('#mobile-nav-toggle i')
															.toggleClass(
																	'fa-times fa-bars');
													$('#mobile-body-overly')
															.fadeOut();
												}
												return false;
											}
										}
									});

					// Porfolio - uses the magnific popup jQuery plugin
					$('.portfolio-popup')
							.magnificPopup(
									{
										type : 'image',
										removalDelay : 300,
										mainClass : 'mfp-fade',
										gallery : {
											enabled : true
										},
										zoom : {
											enabled : true,
											duration : 300,
											easing : 'ease-in-out',
											opener : function(openerElement) {
												return openerElement.is('img') ? openerElement
														: openerElement
																.find('img');
											}
										}
									});

					// Testimonials carousel (uses the Owl Carousel library)
					$(".testimonials-carousel").owlCarousel({
						autoplay : true,
						dots : true,
						loop : true,
						responsive : {
							0 : {
								items : 1
							},
							768 : {
								items : 2
							},
							900 : {
								items : 3
							}
						}
					});

					// Clients carousel (uses the Owl Carousel library)
					$(".clients-carousel").owlCarousel({
						autoplay : true,
						dots : true,
						loop : true,
						responsive : {
							0 : {
								items : 2
							},
							768 : {
								items : 4
							},
							900 : {
								items : 6
							}
						}
					});
				});

var thelmaGpfh = thelmaGpfh || {};

thelmaGpfh.resetForms = function() {
	$('.resettable-control').each(function(i, obj) {
		if ($(obj).hasClass('editor')) {
			$(obj).summernote('reset');
		} else if ($(obj).prop('type') === 'text') {
			$(obj).val('');
		} else if ($(obj).prop('type') === 'number') {
			$(obj).val('');
		} else if ($(obj).prop('type') === 'email') {
			$(obj).val('');
		} else if ($(obj).prop('type') === 'checkbox') {
			$(obj).prop("checked", false);
		} else if ($(obj).hasClass('remove-children')) {
			$(obj).empty();
		}
	});
}

thelmaGpfh.formatMoney = function(value, c, d, t) {
	var n = value, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "."
			: d, t = t == undefined ? "," : t, s = n < 0 ? "-" : "", i = String(parseInt(n = Math
			.abs(Number(n) || 0).toFixed(c))), j = (j = i.length) > 3 ? j % 3
			: 0;
	return '$' + s + (j ? i.substr(0, j) + t : "")
			+ i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
			+ (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
