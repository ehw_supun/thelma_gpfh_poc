$('#add-patient').validator().on('submit', function(e) {
	$('.form-errors').empty();
	if (e.isDefaultPrevented()) {
		// handle the invalid form...
	} else {
		e.preventDefault();
		$('#overlay').fadeIn();
		var patient = thelmaGpfhPatient.validatePatientData();
		if (patient) {
			$.ajax({
				url : contextPath + 'patient/add',
				headers : {
					'Content-Type' : 'application/json'
				},
				method : 'POST',
				dataType : 'json',
				data : patient,
				success : function(data) {
					thelmaGpfh.resetForms();
					$('#create-eposide-yes').attr('href', 'create-episode?patientId=' + data.body)
					$('#patient-added-modal').modal('show');
					$('#overlay').fadeOut();
				},
				error : function(xhr, status, error) {
					$('#overlay').fadeOut();
				}
			});
		}
	}
});

$('#create-episode').validator().on('submit', function(e) {
	$('.form-errors').empty();
	if (e.isDefaultPrevented()) {
		// handle the invalid form...
	} else {
		e.preventDefault();
		$('#overlay').fadeIn();
		var estimate = thelmaGpfhPatient.validateEstimate();
		if (estimate) {
			$.ajax({
				url : contextPath + 'patient/create-estimate',
				headers : {
					'Content-Type' : 'application/json'
				},
				method : 'POST',
				dataType : 'json',
				data : estimate,
				success : function(data) {
					thelmaGpfh.resetForms();
					$('#facility').val('');
					$('#mrn').val('');
					$('#date').val('');
					$('#print-btn').prop('disabled', false);
					$('#add-payment-btn').prop('disabled', false);
					$('#created-estimate-id').val(data.body);
					document.querySelector('#button-panel').scrollIntoView({
				        behavior : 'smooth'
				    });
					$('#overlay').fadeOut();
					$('#estimation-added-modal').modal('show');
				},
				error : function(xhr, status, error) {
					$('#overlay').fadeOut();
				}
			});
		}
	}
});

$('.service-quantity').on(
		'change',
		function(e) {
			var quantityInput = $(this).val();
			var parentTr = $(this).parent().parent();
			var serviceItemValue = parentTr.find(
					'.service-item-unformatted-value').val();
			var itemTotal = serviceItemValue * quantityInput;
			var serviceTotal = parentTr.find('.service-item-total');
			var serviceItemTotalUnformattedValue = parentTr
					.find('.service-item-total-unformatted-value');
			$(serviceItemTotalUnformattedValue).text(itemTotal);
			$(serviceTotal)
					.text(thelmaGpfh.formatMoney(itemTotal, 2, '.', ','));
			console.log(parentTr);
		});

$('.additional-service-quantity').on(
		'change',
		function(e) {
			var quantityInput = $(this).val();
			var parentTr = $(this).parent().parent().parent();
			var serviceItemValue = parentTr.find(
					'.service-item-unformatted-value').val();
			var itemTotal = serviceItemValue * quantityInput;
			var serviceTotal = parentTr.find('.service-item-total');
			var serviceItemTotalUnformattedValue = parentTr
					.find('.selected-service-item-total-unformatted-value');
			$(serviceItemTotalUnformattedValue).text(itemTotal);
			$(serviceTotal)
					.val(thelmaGpfh.formatMoney(itemTotal, 2, '.', ','));
			console.log(parentTr);
		});

$('.add-custom-service').on('click', function(e) {
	var customeItem = {
		itemNumber : $('#custom-service-item-number').val(),
		description : $('#custom-service-desc').val(),
		rate : $('#custom-service-item-rate').val(),
		quantity : $('#custom-service-item-quantity').val(),
		total : $('#custom-service-item-total').val(),
	};
	thelmaGpfhPatient.addNewRow(customeItem);
	thelmaGpfhPatient.updateTotal();
	thelmaGpfh.resetForms();
});

$('.add-service').on('click', function(e) {
	var serviceItem = {
		itemNumber : $('#service-item-number').val(),
		description : $('#service-description').val(),
		rate : $('#unformatted-service-rate').val(),
		quantity : $('#service-quantity-input').val(),
		total : $('.selected-service-item-total-unformatted-value').text(),
	};
	thelmaGpfhPatient.addNewRow(serviceItem, $('#service-type').find(":selected").val());
	thelmaGpfhPatient.updateTotal();
	thelmaGpfh.resetForms();
});

$('#service-items-table').on('change', function(e) {
	thelmaGpfhPatient.updateTotal();
});

$('#print-btn').on('click', function(e) {
	var estimateId = $("#created-estimate-id").val();
	window.open(contextPath + "patient/get-guaranyee-of-payment/" + estimateId, "_new");
});

$('#add-payment-btn').on('click', function(e) {
	var estimateId = $("#created-estimate-id").val();
	window.open(contextPath + "patient/make-payment/" + estimateId, "_new");
});

$('#initiate-gpfh-payment').on('click', function(e) {
	var totalGpfhPayableAmount = parseFloat(0.0);
	$(".gpfh-payment-amount").each(
		function(comp) {
			var thisValue = $(this).val();
			if (thisValue != '' && thisValue > 0) {
				totalGpfhPayableAmount = parseFloat(totalGpfhPayableAmount) + parseFloat(thisValue);
		}
	});	
	if(totalGpfhPayableAmount > 0){
		$.ajax({
            url : contextPath + 'account/create-payment?estimateId='+ $('#estimateId').val() + '&amount=' + totalGpfhPayableAmount,
            headers : {
                'Content-Type' : 'application/json'
            },
            method : 'POST',
            dataType : 'json',
            success : function(data) {
                if (data.status == 200) {
                    window.open(data.body + totalGpfhPayableAmount, '_new');
                } else {
                	alert('Error');
                }
            },
            error : function(xhr, status, error) {
            	alert('Error: ' + error);
            }
        });
	}else{
		alert('Please enter amount');
	}
});

$("#patient-search").on('click', function(e) {
	var parameters = "?firstName=" + $("#firstName").val() + "&lastName=" + $("#lastName").val() + "&mrn=" + $("#mrn").val();
	var ajaxUrl = "search-patient-ajax" + parameters;
	window.history.replaceState("object or string", "Title", window.location.href.split('?')[0] + parameters);
	$("#resultsBlock").load(ajaxUrl);
});

var thelmaGpfhPatient = thelmaGpfhPatient || {};

thelmaGpfhPatient.setServiceInfo = function(service) {
	$('#service-rate').val(
			thelmaGpfh.formatMoney(service.serviceRate, 2, '.', ','));
	$('#unformatted-service-rate').val(service.serviceRate);
	$('#service-item-number').val(service.itemNumber);
	$('#service-description').val(service.serviceDescription);
}

thelmaGpfhPatient.updateTotal = function() {
	var table = $('#service-items-table');
	var total = 0.0;
	$(".service-item").each(
			function(index) {
				var quantityInput = $(this).find('.service-quantity').val();
				if (quantityInput.length > 0 && quantityInput > 0) {
					var serviceItemValue = $(this).find(
							'.service-item-unformatted-value').val();
					var itemTotal = serviceItemValue * quantityInput;
					total = total + itemTotal;
				}
			});
	$('#service-total-display')
			.text(thelmaGpfh.formatMoney(total, 2, '.', ','));
}

thelmaGpfhPatient.addNewRow = function(customService, type) {
	var newRow = $("<tr class='service-item'>");
	var cols = "";

	if(type != undefined && type != ''){
		cols += '<td class="item-code">' + customService.itemNumber + '</td>';
	}else {
		cols += '<td>' + customService.itemNumber + '</td>';
	}
	cols += '<td class="service-item-desc">' + customService.description
			+ '</td>';
	cols += '<td class="service-item-value">'
			+ thelmaGpfh.formatMoney(customService.rate, 2, '.', ',')
			+ '<input class="service-item-unformatted-value" type="hidden" value='
			+ customService.rate + '></td>';
	cols += '<td><input type="number" class="service-quantity" value='
			+ customService.quantity + '>' + '</td>';
	cols += '<td class="service-item-total text-right">'
			+ thelmaGpfh.formatMoney(customService.total, 2, '.', ',')
			+ '</td>';

	newRow.append(cols);
	var tableSection = $("#service-items-table");
	if(type == 'SS'){
		tableSection = $("#specialist-services");
		$(tableSection).show();
	}else if(type == 'PC'){
		tableSection = $("#procedure-services");
		$(tableSection).show();
	}else if(type == 'ANAE'){
		tableSection = $("#anaesthetics-services");
		$(tableSection).show();
	}else if(type == 'PATH'){
		tableSection = $("#pathology-services");
		$(tableSection).show();
	}else if(type == 'MI'){
		tableSection = $("#medical-imaging-services");
		$(tableSection).show();
	}else if(type == 'PROS'){
		tableSection = $("#prosthesis-services");
		$(tableSection).show();
	}
	$(tableSection).append(newRow);
}

thelmaGpfhPatient.validatePatientData = function() {
	var contact = {
		email : $('#email').val(),
		phoneHome : $('#phoneHome').val(),
		phoneMobile : $('#phoneMobile').val(),
		addressLocal : $('#addressLocal').val(),
		addressOverseas : $('#addressOverseas').val()
	};

	var data = {
		firstName : $('#firstName').val(),
		lastName : $('#lastName').val(),
		mrn : $('#mrn').val(),
		passportNumber : $('#passportNumber').val(),
		nextOfKin : $('#nextOfKin').val(),
		nextOfKinRelationship : $('#relationShipToPatient').val(),
		nextOfKinPhoneNumber : $('#nextOfKinPhone').val(),
		dobStr : $('#dob').val(),
		passportCopied : $('#passportCopied').prop("checked"),
		contact : contact
	};
	return JSON.stringify(data);
}

thelmaGpfhPatient.validateEstimate = function() {
	var services = [];
	$('#service-items-table').find('tr').each(function(){
		var quantityInput = $(this).find('.service-quantity');
		if($(quantityInput).length > 0 && $(quantityInput).val() != '' && $(quantityInput).val() > 0){
			services.push({
				itemNumber : $(this).find('.item-code').val() == '' ? $(this).find('.item-code').text() : $(this).find('.item-code').val(),
				quantity : 	parseInt($(quantityInput).val()),
				serviceRate : parseFloat($(this).find('.service-item-unformatted-value').val())
			});
		}
	});
	var estimate = {
	    serviceItems : 	services,
	    patientId : parseInt($("#patientId").val()),
	    facility : $('#facility').val(),
	    mrn : $('#mrn').val(),
	    createdStr : $('#date').val()
	};
	return JSON.stringify(estimate);
}

