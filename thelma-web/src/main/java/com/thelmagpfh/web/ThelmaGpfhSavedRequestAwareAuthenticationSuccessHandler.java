package com.thelmagpfh.web;

import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class ThelmaGpfhSavedRequestAwareAuthenticationSuccessHandler
		extends SavedRequestAwareAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

}
