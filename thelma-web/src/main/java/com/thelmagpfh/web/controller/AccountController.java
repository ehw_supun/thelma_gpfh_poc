package com.thelmagpfh.web.controller;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thelmagpfh.model.dto.AjaxResponse;
import com.thelmagpfh.model.entity.PaymentLink;
import com.thelmagpfh.persistence.dao.PaymentLinkDao;
import com.thelmagpfh.service.patient.AccountService;
import com.thelmagpfh.util.CoreUtil;

@Controller
@RequestMapping("/account")
public class AccountController {

	private @Autowired AccountService accountService;

	@RequestMapping(value = "/gpfh-payment", method = RequestMethod.POST)
	public ResponseEntity<String> gpfhPayment(ModelMap model, @RequestParam Map<String, String> allRequestParams) {
		String paymentId = allRequestParams.get("paymentId");
		String transactionReference = allRequestParams.get("transactionReference");
		System.out.println("Recieved call back : " + allRequestParams);
		if(!CoreUtil.isNull(paymentId)){
			accountService.updatePayment(Integer.valueOf(paymentId), transactionReference);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

//	@RequestMapping(value = "/gpfh-payment", method = RequestMethod.GET)
//	public ResponseEntity<String> gpfhPaymentGet(ModelMap model, @RequestParam Map<String, String> allRequestParams) {
//		return new ResponseEntity<>(HttpStatus.OK);
//	}
	
	@RequestMapping(value = "/success-gpfh-payment", method = RequestMethod.GET)
	public String gpfhPaymentGet(ModelMap model, @RequestParam Map<String, String> allRequestParams) {
		String paymentId = allRequestParams.get("paymentId");
		if(!CoreUtil.isNull(paymentId)){
			Random random = new Random();
			int randomRef = random.nextInt(100000000);
			PaymentLink paymentLink = accountService.findPaymentLinkByPaymentId(Integer.valueOf(paymentId));
			accountService.updatePayment(Integer.valueOf(paymentId), Integer.toString(randomRef));
			
			return "redirect:/patient/view-payments?invoiceId=" + paymentLink.getInvoice().getId();
		}
		return "redirect:/";		
	}

	@ResponseBody
	@RequestMapping(value = "/create-payment", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public AjaxResponse createPayment(@RequestParam Map<String, String> allRequestParams, ModelMap model) {
		String amount = allRequestParams.get("amount");
		String estimateId = allRequestParams.get("estimateId");
		String gpfhUrl = accountService.createPaymentRecord(Integer.valueOf(estimateId), amount);
		return new AjaxResponse(200, gpfhUrl);
	}

}
