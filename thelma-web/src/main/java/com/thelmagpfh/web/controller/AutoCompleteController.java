package com.thelmagpfh.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.thelmagpfh.model.dto.ServiceItem;
import com.thelmagpfh.model.entity.MbsItemCode;
import com.thelmagpfh.model.entity.ProsthesisItemCode;
import com.thelmagpfh.service.CodeService;
import com.thelmagpfh.util.CoreUtil;

@Controller
@RequestMapping("/autocomplete")
public class AutoCompleteController {

	private @Autowired CodeService codeService;

	@RequestMapping(value = "/getMbsCodes", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public @ResponseBody List<ServiceItem> getCities(@RequestParam String code, @RequestParam String type) {
		
		List<ServiceItem> serviceItems = new ArrayList<ServiceItem>();
		if (!CoreUtil.isNull(type) && type.equals("PROS")) {
			List<ProsthesisItemCode> prosthesisItemCodes = codeService.findProsthesisItemCodeByIdorDesc(code);
			for (ProsthesisItemCode serviceItem : prosthesisItemCodes) {
				serviceItems.add(
						new ServiceItem(serviceItem.getCode(), serviceItem.getDescription(), serviceItem.getRate()));
			}
			return serviceItems;
			
		} else {
			List<MbsItemCode> mbsItemCodes = codeService.findMbsItemCodeByIdorDesc(code);
			for (MbsItemCode serviceItem : mbsItemCodes) {
				serviceItems.add(
						new ServiceItem(serviceItem.getCode(), serviceItem.getDescription(), serviceItem.getRate()));
			}
			return serviceItems;
			
		}
		
	}
}
