package com.thelmagpfh.web.controller;

import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.thelmagpfh.model.dto.AjaxResponse;
import com.thelmagpfh.model.dto.PatientSearchCriteria;
import com.thelmagpfh.model.entity.Estimate;
import com.thelmagpfh.model.entity.Patient;
import com.thelmagpfh.persistence.dao.PatientSearchCriteriaSpecification;
import com.thelmagpfh.service.patient.AccountService;
import com.thelmagpfh.service.patient.PatientService;
import com.thelmagpfh.util.CoreUtil;

@Controller
@RequestMapping("/patient")
public class PatientController {

	private @Autowired PatientService patientService;
	
	private @Autowired AccountService accountService; 

	@RequestMapping("/add")
	public ModelAndView login() {
		return new ModelAndView("patient/add-patient");
	}

	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public AjaxResponse addPatient(@RequestBody Patient patient, ModelMap model, HttpSession httpSession) {
		if (patient != null) {
			patient.setDateOfBirth(CoreUtil.parseDate(patient.getDobStr()));
		}
		Patient saved = patientService.addPatient(patient);
		return new AjaxResponse(200, saved.getId().toString());
	}

	@RequestMapping("/create-episode")
	public ModelAndView addEpisode(ModelMap model, @RequestParam Map<String, String> allRequestParams) {

		model.addAttribute("fixedServiceItems", patientService.getFixedServiceItems());
		String patientId = allRequestParams.get("patientId");
		if (!CoreUtil.isNull(patientId)) {
			model.addAttribute("patient", patientService.findById(Integer.valueOf(patientId)));
		}

		return new ModelAndView("patient/create-episode");
	}

	@ResponseBody
	@RequestMapping(value = "/create-estimate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public AjaxResponse createEstimate(@RequestBody Estimate estimate, ModelMap model, HttpSession httpSession) {

		if (!CoreUtil.isNull(estimate)) {
			estimate.setCreated(CoreUtil.parseDate(estimate.getCreatedStr()));
			patientService.createEstimate(estimate);
		}
		return new AjaxResponse(200, Integer.toString(estimate.getId()));
	}

	@RequestMapping(value = "/get-guaranyee-of-payment/{estimateId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getReceipt(@PathVariable Integer estimateId) {
		Estimate estimate = patientService.getEstimate(estimateId);
		if (estimate != null) {
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_PDF);
			byte[] documentContentByteArray = null;
			try {
				documentContentByteArray = estimate.getContent().getBytes(1, (int) estimate.getContent().length());
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return new ResponseEntity<byte[]>(documentContentByteArray, headers, HttpStatus.CREATED);
		}
		return null;

	}

	@RequestMapping("/make-payment/{estimateId}")
	public String makePayment(ModelMap model, @PathVariable Integer estimateId) {
		Estimate estimate = patientService.getEstimate(estimateId);
		model.addAttribute("total", estimate.getInvoice().getAmount().doubleValue());
		model.addAttribute("estimateId", estimateId);
		return "patient/make-payment";
	}

	@RequestMapping("/search-patient")
	public String searchPatient(ModelMap model) {
		return "patient/search-patient";
	}

	@RequestMapping(value = "/search-patient-ajax", method = RequestMethod.GET)
	public String searchPatientAjax(@RequestParam Map<String, String> allRequestParams, ModelMap model,
			@PageableDefault(sort = { "id" }, direction = Direction.DESC, size = 10) Pageable pageable) {
		String firstName = allRequestParams.get("firstName");
		String lastName = allRequestParams.get("lastName");
		String mrn = allRequestParams.get("mrn");
		PatientSearchCriteria patientSearchCriteria = new PatientSearchCriteria();
		patientSearchCriteria.setFirstName(firstName);
		patientSearchCriteria.setLastName(lastName);
		patientSearchCriteria.setMrn(mrn);
		PatientSearchCriteriaSpecification<PatientSearchCriteria> criteriaSpecification = new PatientSearchCriteriaSpecification<>(
				patientSearchCriteria);
		model.addAttribute("patients", patientService.findPatients(criteriaSpecification, pageable));
		return "patient/search-patient-results";
		
	}
	
	@RequestMapping("/view-estimates")
	public String uppaidEstimates(ModelMap model, @RequestParam Map<String, String> allRequestParams) {
		String patientId = allRequestParams.get("patientId");
		model.addAttribute("estimates", patientService.findEstimatesByPatientId(Integer.valueOf(patientId)));
		model.addAttribute("patient", patientService.findById(Integer.valueOf(patientId)));
		return "patient/view-estimates";
	}

	@RequestMapping("/view-payments")
	public String viewPayments(ModelMap model, @RequestParam(name = "invoiceId") int invoiceId) {
		model.addAttribute("payments", accountService.findCompletedPaymentsByInvoiceId(invoiceId));
		return "patient/view-payments";
	}

}
