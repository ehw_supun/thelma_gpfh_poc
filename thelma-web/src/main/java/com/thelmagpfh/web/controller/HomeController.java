package com.thelmagpfh.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {

	private final Log LOGGER = LogFactory.getLog(getClass());

	@RequestMapping
	public String home() {
		return "redirect:/home";
	}

	@RequestMapping("/index")
	public String index() {
		return "redirect:/home";
	}

	@RequestMapping("/login")
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("errorMsg", "Invalid username or password");

		if (logout != null)
			model.addAttribute("msg", "You have been logged out successfully.");
		return "login";
	}

	@RequestMapping("/home")
	public ModelAndView homePage() {
		return new ModelAndView("home");
	}

}
