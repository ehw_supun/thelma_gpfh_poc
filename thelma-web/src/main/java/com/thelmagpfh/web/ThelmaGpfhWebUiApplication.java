package com.thelmagpfh.web;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.dialect.springdata.SpringDataDialect;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.thelmagpfh.web.thymeleaf.DatabaseTemplateResolver;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@SpringBootApplication(exclude = { org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration.class })
@EnableAsync
@EnableScheduling
@ComponentScan("com.thelmagpfh")
@EntityScan("com.thelmagpfh.model")
@EnableJpaRepositories("com.thelmagpfh")
public class ThelmaGpfhWebUiApplication extends org.springframework.boot.web.support.SpringBootServletInitializer
		implements ApplicationContextAware{

	private @Autowired AutowireCapableBeanFactory beanFactory;
	
	private @Autowired DatabaseTemplateResolver databaseTemplateResolver;

	private ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@Bean
	public ViewResolver viewResolver() {
		ThymeleafViewResolver resolver = new ThymeleafViewResolver();
		resolver.setTemplateEngine(templateEngine());
		resolver.setCharacterEncoding("UTF-8");
		return resolver;
	}

	@Bean
	public TemplateEngine templateEngine() {
		org.thymeleaf.spring4.SpringTemplateEngine engine = new org.thymeleaf.spring4.SpringTemplateEngine();
		engine.setEnableSpringELCompiler(true);
		engine.setTemplateResolvers(new HashSet<ITemplateResolver>() {
			/**
			* 
			*/
			private static final long serialVersionUID = 1L;

			{
				add(templateResolver());
				databaseTemplateResolver.setCheckExistence(false);
				databaseTemplateResolver.setOrder(2);
				add(databaseTemplateResolver);
			}
		});
		engine.addDialect(new LayoutDialect());
		engine.addDialect(new SpringDataDialect());
		engine.addDialect(new SpringSecurityDialect());
		return engine;
	}

	@Bean
	public ITemplateResolver templateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext(applicationContext);
		resolver.setPrefix("classpath:/templates/");
		resolver.setSuffix(".html");
		resolver.setTemplateMode(TemplateMode.HTML);
		resolver.setOrder(1);
		resolver.setCheckExistence(true);
		return resolver;
	}

	@Bean(name = "appProperties")
	public Properties appProperties() throws IOException {
		Properties props = new Properties();
		props.load(getClass().getClassLoader().getResourceAsStream("application.properties"));
		return props;
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(applicationClass);
	}
	
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ThelmaGpfhWebUiApplication.class, args);
    }

	private static Class<ThelmaGpfhWebUiApplication> applicationClass = ThelmaGpfhWebUiApplication.class;


}
