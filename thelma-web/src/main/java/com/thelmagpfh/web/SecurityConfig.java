package com.thelmagpfh.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
				.antMatchers("/account/**", "/img/**", "/imgage/**", "/imgages/**", "/common/**", "/css/**", "/fonts/**",
						"/js/**", "/lib/**", "favicon.ico")
				.permitAll().and().formLogin().loginPage("/login").successHandler(successHandler()).and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login").permitAll().and()
				.authorizeRequests().anyRequest().authenticated();
		
	}

	private AuthenticationSuccessHandler successHandler() {
		ThelmaGpfhSavedRequestAwareAuthenticationSuccessHandler successHandler = new ThelmaGpfhSavedRequestAwareAuthenticationSuccessHandler() {
			private RequestCache requestCache = new HttpSessionRequestCache();

			@Override
			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
					Authentication authentication) throws ServletException, IOException {
				SavedRequest savedRequest = requestCache.getRequest(request, response);
				String redirectUrl = null;
				if (savedRequest == null) {
					super.onAuthenticationSuccess(request, response, authentication);
				} else {
					String targetUrlParameter = getTargetUrlParameter();
					if (isAlwaysUseDefaultTargetUrl() || (targetUrlParameter != null
							&& StringUtils.hasText(request.getParameter(targetUrlParameter)))) {
						requestCache.removeRequest(request, response);
						super.onAuthenticationSuccess(request, response, authentication);
					}

					// Use the DefaultSavedRequest URL
					java.net.URL url = new java.net.URL(savedRequest.getRedirectUrl());
					redirectUrl = url.getPath().replace(request.getContextPath(), "") + (url.getQuery() == null ? "" : "?" + url.getQuery());
					
					
				}
				getRedirectStrategy().sendRedirect(request, response, redirectUrl);
			}
		};
		return successHandler;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("gpfh").password("gpfh").roles("USER");
	}
}
