package com.thelmagpfh.web.thymeleaf;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashSet;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.cache.ICacheEntryValidity;
import org.thymeleaf.cache.NonCacheableCacheEntryValidity;
import org.thymeleaf.exceptions.TemplateInputException;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.AbstractTemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;
import org.thymeleaf.util.LoggingUtils;
import org.thymeleaf.util.Validate;

import com.thelmagpfh.model.entity.EmailTemplate;
import com.thelmagpfh.persistence.dao.EmailTemplateDao;

@Component
public class DatabaseTemplateResolver extends AbstractTemplateResolver {

	private @Autowired EmailTemplateDao emailTemplateDao;

	private TemplateMode templateMode = TemplateMode.HTML;

	public DatabaseTemplateResolver() {
		super();
		super.setResolvablePatterns(new HashSet<String>() {

			private static final long serialVersionUID = 1L;

			{
				add("db:*");
			}
		});
	}

	@Override
	protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate,
			String template, Map<String, Object> templateResolutionAttributes) {
		return new DatabaseTemplateResource(template);
	}

	@Override
	protected TemplateMode computeTemplateMode(IEngineConfiguration configuration, String ownerTemplate,
			String template, Map<String, Object> templateResolutionAttributes) {
		return templateMode;
	}

	@Override
	protected ICacheEntryValidity computeValidity(IEngineConfiguration configuration, String ownerTemplate,
			String template, Map<String, Object> templateResolutionAttributes) {
		return NonCacheableCacheEntryValidity.INSTANCE;
	}

	private final class DatabaseTemplateResource implements ITemplateResource {

		private final String resource;

		public DatabaseTemplateResource(final String resource) {
			super();
			Validate.notNull(resource, "Resource cannot be null or empty");
			this.resource = resource;
		}

		public String getDescription() {
			return this.resource;
		}

		public String getBaseName() {
			// This kind of resource cannot be used for computing derivative
			// names from its base
			return null;
		}

		public Reader reader() throws IOException {
			EmailTemplate emailTemplate = emailTemplateDao.findByName(this.resource.split("db:")[1]);
			return emailTemplate != null ? new StringReader(emailTemplate.getValue()) : null;
		}

		public ITemplateResource relative(final String relativeLocation) {
			throw new TemplateInputException(
					String.format("Cannot create a relative resource for String resource  \"%s\"",
							LoggingUtils.loggifyTemplateName(this.resource)));
		}

		public boolean exists() {
			return true;
		}

	}
}
