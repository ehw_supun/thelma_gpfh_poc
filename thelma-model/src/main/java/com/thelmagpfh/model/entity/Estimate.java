package com.thelmagpfh.model.entity;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.thelmagpfh.model.dto.ServiceItem;

@Entity
public class Estimate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;

	@Column
	private String facility;

	@Column
	private String mrn;

	@Column
	private Date created;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "INVOICE_ID", foreignKey = @ForeignKey(name = "FK_ESTIMATE_INVOICE"))
	private Invoice invoice;
	
	@ManyToOne
	@JoinColumn(name = "PATIENT_ID", foreignKey = @ForeignKey(name = "FK_ESTIMATE_PATIENT"))
	private Patient patient;
	
    @Column
    private Blob content;

	@Transient
	private Integer patientId;

	@Transient
	private List<ServiceItem> serviceItems;
	
	@Transient
	private String createdStr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public List<ServiceItem> getServiceItems() {
		return serviceItems;
	}

	public void setServiceItems(List<ServiceItem> serviceItems) {
		this.serviceItems = serviceItems;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public String getCreatedStr() {
		return createdStr;
	}

	public void setCreatedStr(String createdStr) {
		this.createdStr = createdStr;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Blob getContent() {
		return content;
	}

	public void setContent(Blob content) {
		this.content = content;
	}
}
