package com.thelmagpfh.model.entity;

public enum PaymentStatus {

	CREATED, COMPLETED, CANCELLED
	
}
