package com.thelmagpfh.model.entity;

public enum InvoiceStatus {
	UNPAID, PAID
}
