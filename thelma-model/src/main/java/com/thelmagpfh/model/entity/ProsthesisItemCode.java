package com.thelmagpfh.model.entity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@Cacheable
@PrimaryKeyJoinColumn(name = "CODE")
public class ProsthesisItemCode extends ServiceCode {

	@Column
	private Double rate;

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
}
