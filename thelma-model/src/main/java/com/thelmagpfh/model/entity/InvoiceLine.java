package com.thelmagpfh.model.entity;

import java.math.BigDecimal;

import javax.persistence.*;

@Entity
public class InvoiceLine {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "CODE", foreignKey = @ForeignKey(name = "FK_ESTIMATE_CODE"))
	private ServiceCode code;

	@Column
	private Integer quantity;

	@Column
	private BigDecimal amount;

	@ManyToOne
	@JoinColumn(name = "INVOICE_ID", foreignKey = @ForeignKey(name = "FK_INVOICE_LINE_INVOICE"))
	private Invoice invoice;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ServiceCode getCode() {
		return code;
	}

	public void setCode(ServiceCode code) {
		this.code = code;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}
