package com.thelmagpfh.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;
	
	@Column
	private String email;
	
	@Column
	private String addressLocal;
	
	@Column
	private String addressOverseas;
	
	@Column
	private String phoneHome;
	
	@Column
	private String phoneMobile;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressLocal() {
		return addressLocal;
	}

	public void setAddressLocal(String addressLocal) {
		this.addressLocal = addressLocal;
	}

	public String getAddressOverseas() {
		return addressOverseas;
	}

	public void setAddressOverseas(String addressOverseas) {
		this.addressOverseas = addressOverseas;
	}

	public String getPhoneHome() {
		return phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getPhoneMobile() {
		return phoneMobile;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}
}
