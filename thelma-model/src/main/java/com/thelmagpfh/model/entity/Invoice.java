package com.thelmagpfh.model.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Invoice {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;

	@Column
	private Date created;

	@OneToMany(mappedBy = "invoice", targetEntity = InvoiceLine.class, cascade = {
			CascadeType.ALL }, orphanRemoval = true)
	private Set<InvoiceLine> invoiceLines;

	@Column
	private BigDecimal amount;
	
	@Transient
	private BigDecimal paidAmount;

	@Enumerated(EnumType.STRING)
	private InvoiceStatus status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Set<InvoiceLine> getInvoiceLines() {
		return invoiceLines;
	}

	public void setInvoiceLines(Set<InvoiceLine> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal calculateTotal() {
		BigDecimal total = BigDecimal.ZERO;
		if (this.invoiceLines != null) {
			for (InvoiceLine invoiceLine : invoiceLines) {
				total = total.add(invoiceLine.getAmount());
			}
		}
		return total;
	}

	public InvoiceStatus getStatus() {
		return status;
	}

	public void setStatus(InvoiceStatus status) {
		this.status = status;
	}

	public BigDecimal getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(BigDecimal paidAmount) {
		this.paidAmount = paidAmount;
	}

	public boolean isPaid() {
		return status != null && InvoiceStatus.PAID.equals(status);
	}

}
