package com.thelmagpfh.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.persistence.ForeignKey;

@Entity
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", columnDefinition = "bigint unsigned")
	private Integer id;

	@Column
	private String firstName;

	@Column
	private String lastName;
	
	@Column
	private String mrn;

	@Column
	private Date dateOfBirth;

	@Column
	private String passportNumber;

	@Column
	private Boolean passportCopied;

	@Column
	private String nextOfKin;

	@Column
	private String nextOfKinRelationship;

	@Column
	private String nextOfKinPhoneNumber;

	@OneToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTACT_ID", foreignKey = @ForeignKey(name = "FK_PATIENT_CONTACT"))
	private Contact contact;
	
	@Transient
	private String dobStr;
	
	@Transient
	private boolean unpaidInvoices;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Boolean getPassportCopied() {
		return passportCopied;
	}

	public void setPassportCopied(Boolean passportCopied) {
		this.passportCopied = passportCopied;
	}

	public String getNextOfKin() {
		return nextOfKin;
	}

	public void setNextOfKin(String nextOfKin) {
		this.nextOfKin = nextOfKin;
	}

	public String getNextOfKinRelationship() {
		return nextOfKinRelationship;
	}

	public void setNextOfKinRelationship(String nextOfKinRelationship) {
		this.nextOfKinRelationship = nextOfKinRelationship;
	}

	public String getNextOfKinPhoneNumber() {
		return nextOfKinPhoneNumber;
	}

	public void setNextOfKinPhoneNumber(String nextOfKinPhoneNumber) {
		this.nextOfKinPhoneNumber = nextOfKinPhoneNumber;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getDobStr() {
		return dobStr;
	}

	public void setDobStr(String dobStr) {
		this.dobStr = dobStr;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public boolean isUnpaidInvoices() {
		return unpaidInvoices;
	}

	public void setUnpaidInvoices(boolean unpaidInvoices) {
		this.unpaidInvoices = unpaidInvoices;
	}
}
