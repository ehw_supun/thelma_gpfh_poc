package com.thelmagpfh.model.entity;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class ServiceCode {

	public final static String ID_PROP = "id";
	public final static String VALUE_PROP = "value";
	public final static String DESCRIPTION_PROP = "description";
	public final static String DISPLAY_ORDER_PROP = "displayOrder";
	public final static String INVALIDATION_DATE_PROP = "invalidationDate";

	public final static int CODE_LENGTH = 50;
	public final static int VALUE_LENGTH = 100;

	@Transient
	protected boolean changeCaseAllowed = true;

	@Id
	@Column(name = "CODE")
	@Length(max = CODE_LENGTH)
	@NotNull
	private String id;

	@Column(name = "DESCRIPTION")
	@Length(max = 500)
	private String description;

	@Column(name = "DISPLAY_ORDER")
	@NotNull
	private Integer displayOrder;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getCode() {
		return id;
	}

	public void setCode(String id) {
		this.id = id;
	}

	public boolean getChangeCaseAllowed() {
		return changeCaseAllowed;
	}

	public void setChangeCaseAllowed(boolean changeCaseAllowed) {
		this.changeCaseAllowed = changeCaseAllowed;
	}

	public static final Comparator<ServiceCode> DISPLAY_ORDER_COMPARATOR = new Comparator<ServiceCode>() {

		public int compare(ServiceCode obj1, ServiceCode obj2) {
			return obj1.getDisplayOrder().compareTo(obj2.getDisplayOrder());
		}
	};
}
