package com.thelmagpfh.model.dto;

public class ServiceItem {
	
	private Integer itemCode;
	
	private String itemNumber;
	
	private String serviceDescription;
	
	private double serviceRate;
	
	private int quantity;
	
	private double itemTotal;
	
	public ServiceItem() {

	}
	
	public ServiceItem(String itemNumber, String serviceDescription, double serviceRate) {
		this.itemNumber = itemNumber;
		this.serviceDescription = serviceDescription;
		this.serviceRate = serviceRate;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public double getServiceRate() {
		return serviceRate;
	}

	public void setServiceRate(double serviceRate) {
		this.serviceRate = serviceRate;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getItemTotal() {
		return itemTotal;
	}

	public void setItemTotal(double itemTotal) {
		this.itemTotal = itemTotal;
	}

}
