package com.thelmagpfh.model.dto;

public class AjaxResponse {


	public static AjaxResponse _200_OK = new AjaxResponse(200, "Ok");

	public static AjaxResponse _417_EXPECTATION_FAILED = new AjaxResponse(417, "Expectation Failed");

	private int status;
	private String body;
	private String url;

	public AjaxResponse() {
		super();
	}

	public AjaxResponse(int status, String body) {
		super();
		this.status = status;
		this.body = body;
	}

	public AjaxResponse(int status, String body, String url) {
		super();
		this.status = status;
		this.body = body;
		this.url = url;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


}
