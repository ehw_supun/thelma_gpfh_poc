package com.thelmagpfh.service;

import java.util.List;

import com.thelmagpfh.model.entity.ServiceCode;
import com.thelmagpfh.model.entity.FixedServiceItemCode;
import com.thelmagpfh.model.entity.MbsItemCode;
import com.thelmagpfh.model.entity.ProsthesisItemCode;

public interface CodeService {

	List<MbsItemCode> findMbsItemCodeByIdorDesc(String q);
	
	List<FixedServiceItemCode> findAllFixedServiceItemCode();
	
	ServiceCode findById(String code);

	List<ProsthesisItemCode> findProsthesisItemCodeByIdorDesc(String code);
}
