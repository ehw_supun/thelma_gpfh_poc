package com.thelmagpfh.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.thelmagpfh.model.entity.ServiceCode;
import com.thelmagpfh.model.entity.FixedServiceItemCode;
import com.thelmagpfh.model.entity.MbsItemCode;
import com.thelmagpfh.model.entity.ProsthesisItemCode;
import com.thelmagpfh.persistence.dao.ServiceCodeDao;
import com.thelmagpfh.persistence.dao.FixedServiceItemCodeDao;
import com.thelmagpfh.persistence.dao.MbsItemCodeDao;
import com.thelmagpfh.persistence.dao.ProsthesisItemCodeDao;

@Component
public class CodeServiceImpl implements CodeService {

	private @Autowired MbsItemCodeDao mbsItemCodeDao;

	private @Autowired FixedServiceItemCodeDao fixedServiceItemCodeDao;
	
	private @Autowired ServiceCodeDao serviceCodeDao;
	
	private @Autowired ProsthesisItemCodeDao prosthesisItemCodeDao;

	@Override
	public List<MbsItemCode> findMbsItemCodeByIdorDesc(String q) {
		return mbsItemCodeDao.findMbsItemCodeByIdorDesc(q);
	}

	@Override
	public List<FixedServiceItemCode> findAllFixedServiceItemCode() {
		return fixedServiceItemCodeDao.findAll();
	}

	@Override
	public ServiceCode findById(String code) {
		return serviceCodeDao.findById(code);
	}

	@Override
	public List<ProsthesisItemCode> findProsthesisItemCodeByIdorDesc(String code) {
		return prosthesisItemCodeDao.findProsthesisItemCodeByIdorDesc(code);
	}

}
