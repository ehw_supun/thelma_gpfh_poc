package com.thelmagpfh.service.patient;

import java.math.BigDecimal;
import java.util.List;

import com.thelmagpfh.model.entity.Estimate;
import com.thelmagpfh.model.entity.Payment;
import com.thelmagpfh.model.entity.PaymentLink;

public interface AccountService {

	Payment createPayment(Payment payment);

	String createPaymentRecord(Integer estimateId, String amount);

	String getGpfhUrl(Estimate estimate, Payment payment);

	void updatePayment(Integer valueOf, String reference);

	BigDecimal getPaidTotalAmount(Integer invoiceId);

	List<Payment> findCompletedPaymentsByInvoiceId(int invoiceId);
	
	PaymentLink findPaymentLinkByPaymentId(Integer paymentId);
}
