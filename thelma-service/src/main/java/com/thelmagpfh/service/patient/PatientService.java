package com.thelmagpfh.service.patient;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.thelmagpfh.model.dto.PatientSearchCriteria;
import com.thelmagpfh.model.dto.ServiceItem;
import com.thelmagpfh.model.entity.Estimate;
import com.thelmagpfh.model.entity.Patient;
import com.thelmagpfh.persistence.dao.PatientSearchCriteriaSpecification;

public interface PatientService {

	Patient addPatient(Patient patient);
	
	List<ServiceItem> getFixedServiceItems();
	
	Patient findById(Integer id);

	Estimate createEstimate(Estimate estimate);

	Estimate getEstimate(Integer id);
	
	List<Patient> findPatients(PatientSearchCriteriaSpecification<PatientSearchCriteria> criteriaSpecification, Pageable pageable);

	List<Estimate> findEstimatesByPatientId(Integer patientId);
}
