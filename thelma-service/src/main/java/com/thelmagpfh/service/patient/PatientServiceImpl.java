package com.thelmagpfh.service.patient;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.thelmagpfh.model.dto.PatientSearchCriteria;
import com.thelmagpfh.model.dto.ServiceItem;
import com.thelmagpfh.model.entity.ServiceCode;
import com.thelmagpfh.model.entity.Estimate;
import com.thelmagpfh.model.entity.FixedServiceItemCode;
import com.thelmagpfh.model.entity.Invoice;
import com.thelmagpfh.model.entity.InvoiceLine;
import com.thelmagpfh.model.entity.Patient;
import com.thelmagpfh.persistence.dao.ContactDao;
import com.thelmagpfh.persistence.dao.EstimateDao;
import com.thelmagpfh.persistence.dao.PatientDao;
import com.thelmagpfh.persistence.dao.PatientSearchCriteriaSpecification;
import com.thelmagpfh.service.CodeService;
import com.thelmagpfh.util.CoreUtil;

@Transactional
@Component
public class PatientServiceImpl implements PatientService, ApplicationListener<ContextRefreshedEvent> {

	private @Autowired PatientDao patientDao;

	private @Autowired ContactDao contactDao;

	private @Autowired CodeService codeSevice;

	private @Autowired EstimateDao estimateDao;

	private @Autowired DocumentService documentService;

	private @Autowired AccountService accountService;

	private List<ServiceItem> fixedServiceItems = new ArrayList<ServiceItem>();

	@Override
	public Patient addPatient(Patient patient) {
		contactDao.save(patient.getContact());
		return patientDao.save(patient);
	}

	@Override
	public List<ServiceItem> getFixedServiceItems() {
		return fixedServiceItems;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		List<FixedServiceItemCode> fixedServiceItemCodes = codeSevice.findAllFixedServiceItemCode();
		for (FixedServiceItemCode itemCode : fixedServiceItemCodes) {
			fixedServiceItems.add(new ServiceItem(itemCode.getCode(), itemCode.getDescription(), itemCode.getRate()));
		}
	}

	@Override
	public Patient findById(Integer id) {
		return patientDao.findById(id);
	}

	@Override
	public Estimate createEstimate(Estimate estimate) {
		Invoice invoice = new Invoice();
		Set<InvoiceLine> invoiceLines = createInvoiceLines(estimate.getServiceItems(), invoice);
		invoice.setInvoiceLines(invoiceLines);
		invoice.setAmount(invoice.calculateTotal());
		invoice.setCreated(new Date());

		estimate.setInvoice(invoice);
		estimate.setPatient(patientDao.findById(estimate.getPatientId()));
		documentService.generateEstimatePdf(estimate);
		return estimateDao.save(estimate);
	}

	private Set<InvoiceLine> createInvoiceLines(List<ServiceItem> serviceItems, Invoice invoice) {
		Set<InvoiceLine> invoiceLines = new HashSet<InvoiceLine>();
		for (ServiceItem serviceItem : serviceItems) {
			InvoiceLine invoiceLine = new InvoiceLine();
			if (!CoreUtil.isNull(serviceItem.getItemNumber())) {
				ServiceCode code = codeSevice.findById(serviceItem.getItemNumber());
				invoiceLine.setCode(code);
			}
			invoiceLine.setQuantity(serviceItem.getQuantity());
			invoiceLine.setAmount(BigDecimal.valueOf(serviceItem.getQuantity() * serviceItem.getServiceRate()));
			invoiceLine.setInvoice(invoice);
			invoiceLines.add(invoiceLine);
		}
		return invoiceLines;
	}

	@Override
	public Estimate getEstimate(Integer id) {
		return estimateDao.findById(id);
	}

	@Override
	public List<Patient> findPatients(PatientSearchCriteriaSpecification<PatientSearchCriteria> criteriaSpecification,
			Pageable pageable) {
		Page<Patient> patients = patientDao.findAll(criteriaSpecification, pageable);
		List<Patient> patientList = new ArrayList<Patient>();
		
		for (Patient patient : patients) {
			patientList.add(patient);
		}
		
		List<Estimate> estimates = estimateDao.findByPatientIn(patientList);
		Map<Integer, List<Estimate>> patientEstimates = new HashMap<Integer, List<Estimate>>();
		for (Estimate estimate : estimates) {
			List<Estimate> estimateList = patientEstimates.get(estimate.getPatient().getId());
			if (estimateList == null) {
				estimateList = new ArrayList<Estimate>();
			}
			estimateList.add(estimate);
			patientEstimates.put(estimate.getPatient().getId(), estimateList);
		}

		for (Patient patient : patientList) {
			updateUnpaidInvoicesStatus(patient, patientEstimates.get(patient.getId()));
		}
		return patientList;
	}

	private void updateUnpaidInvoicesStatus(Patient patient, List<Estimate> estimates) {
		if (!CoreUtil.isNull(estimates)) {
			for (Estimate estimate : estimates) {
				Invoice invoice = estimate.getInvoice();
				if (!invoice.isPaid()) {
					patient.setUnpaidInvoices(Boolean.TRUE);
					return;
				}
			}
		}
		patient.setUnpaidInvoices(Boolean.FALSE);
	}

	@Override
	public List<Estimate> findEstimatesByPatientId(Integer patientId) {
		List<Estimate> estimates = estimateDao.findByPatientId(patientId);
		for(Estimate estimate : estimates){
			estimate.getInvoice().setPaidAmount(accountService.getPaidTotalAmount(estimate.getInvoice().getId()));
		}
		return estimates;
	}

}
