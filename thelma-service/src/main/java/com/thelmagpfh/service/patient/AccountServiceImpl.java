package com.thelmagpfh.service.patient;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.thelmagpfh.model.entity.Estimate;
import com.thelmagpfh.model.entity.InvoiceStatus;
import com.thelmagpfh.model.entity.Payment;
import com.thelmagpfh.model.entity.PaymentLink;
import com.thelmagpfh.model.entity.PaymentStatus;
import com.thelmagpfh.persistence.dao.EstimateDao;
import com.thelmagpfh.persistence.dao.PaymentDao;
import com.thelmagpfh.persistence.dao.PaymentLinkDao;

@Component
public class AccountServiceImpl implements AccountService {

	@Value("${wubs.gpfh.url}")
	private String gpfhUrl;

	@Value("${wubs.gpfh.clientId}")
	private String gpfhClientId;

	@Value("${wubs.gpfh.serviceId}")
	private String gpfhServiceId;

	@Value("${wubs.gpfh.callback.url}")
	private String gpfhCallbackUrl;
	
	@Value("${wubs.gpfh.return.url}")
	private String returnUrl;

	private @Autowired PaymentDao paymentDao;

	private @Autowired EstimateDao estimateDao;

	private @Autowired PaymentLinkDao paymentLinkDao;

	@Override
	public Payment createPayment(Payment payment) {
		return paymentDao.save(payment);
	}

	@Override
	public String createPaymentRecord(Integer estimateId, String amount) {
		Payment payment = new Payment();
		payment.setAmount(BigDecimal.valueOf(Double.valueOf(amount)));
		payment.setCreated(new Date());
		payment.setStatus(PaymentStatus.CREATED);
		createPayment(payment);

		Estimate estimate = estimateDao.findById(estimateId);

		PaymentLink paymentLink = new PaymentLink();
		paymentLink.setPayment(payment);
		paymentLink.setInvoice(estimate.getInvoice());

		paymentLinkDao.save(paymentLink);
		return getGpfhUrl(estimate, payment);
	}

	@Override
	public String getGpfhUrl(Estimate estimate, Payment payment) {
		StringBuffer url = new StringBuffer();
		url.append(gpfhUrl);
		url.append("clientId=");
		url.append(gpfhClientId);
		url.append("&service1.id=");
		url.append(gpfhServiceId);
		url.append("&buyer.id=");
		url.append(estimate.getPatient().getMrn());
		url.append("&customField=");
		url.append(estimate.getId());
		url.append("&buyer.firstName=");
		url.append(estimate.getPatient().getFirstName());
		url.append("&buyer.lastName=");
		url.append(estimate.getPatient().getLastName());
		url.append("&buyer.email=");
		url.append(estimate.getPatient().getContact().getEmail());
		url.append("&callbackUrl=");
		url.append(gpfhCallbackUrl);
		url.append("&callbackParamName.1=");
		url.append("paymentId");
		url.append("&callbackParamValue.1=");
		url.append(payment.getId());
		url.append("&callbackMethod=");
		url.append("POST");
		url.append("&returnUrl=");
		url.append(returnUrl + "?paymentId=" + payment.getId());
		url.append("&service1.amount=");
		System.out.println(url);
		return url.toString();
	}

	@Override
	public void updatePayment(Integer paymentId, String transactionReference) {
		PaymentLink paymentLink = paymentLinkDao.findByPaymentId(paymentId);
		if (paymentLink != null) {
			paymentLink.getPayment().setStatus(PaymentStatus.COMPLETED);
			paymentLink.getPayment().setReferenceNumber(transactionReference);
			BigDecimal currentPaidAmount = getPaidTotalAmount(paymentLink.getInvoice().getId());
			BigDecimal totalPaidAmount = currentPaidAmount.add(paymentLink.getPayment().getAmount());
			if (paymentLink.getInvoice().getAmount().compareTo(totalPaidAmount) > 0) {
				paymentLink.getInvoice().setStatus(InvoiceStatus.UNPAID);
			} else{
				paymentLink.getInvoice().setStatus(InvoiceStatus.PAID);
			}
			paymentLinkDao.save(paymentLink);
		}
	}

	@Override
	public BigDecimal getPaidTotalAmount(Integer invoiceId) {
		BigDecimal totalPaidAmount = paymentLinkDao.totalPaymentAmount(invoiceId, PaymentStatus.COMPLETED);
		return totalPaidAmount == null ? BigDecimal.ZERO : totalPaidAmount;
	}

	@Override
	public List<Payment> findCompletedPaymentsByInvoiceId(int invoiceId) {
		return paymentLinkDao.findCompletedPaymentsByInvoiceId(invoiceId, PaymentStatus.COMPLETED);
	}

	@Override
	public PaymentLink findPaymentLinkByPaymentId(Integer paymentId) {
		return paymentLinkDao.findByPaymentId(paymentId);
	}
}
