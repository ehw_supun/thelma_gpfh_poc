package com.thelmagpfh.service.patient;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;

import javax.sql.rowset.serial.SerialBlob;

import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.pdf.BaseFont;
import com.thelmagpfh.model.entity.Estimate;

@Component
public class DocumentServiceImpl implements DocumentService {

	@Override
	public void generateEstimatePdf(Estimate estimate) {
		ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
		resolver.setTemplateMode(TemplateMode.HTML);
		resolver.setSuffix(".html");
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(resolver);

		StringWriter guaranteeWriter = new StringWriter();
		Context context = new Context();
		context.setVariable("estimate", estimate);
		templateEngine.process("guarantee", context, guaranteeWriter);
		String guaranteeHtml = guaranteeWriter.toString();

		final ITextRenderer renderer = new ITextRenderer();
		ITextFontResolver fontResolver = renderer.getFontResolver();
		try {
			fontResolver.addFont("Verdana.ttf", BaseFont.EMBEDDED);
		} catch (Exception e) {

		}

		byte[] byteArray = new byte[0];
		try (ByteArrayOutputStream fos = new ByteArrayOutputStream()) {
			renderer.setDocumentFromString(guaranteeHtml);
			renderer.layout();
			renderer.createPDF(fos, false);
			StringWriter estimateWriter = new StringWriter();
			templateEngine.process("estimate", context, estimateWriter);
			String estimateHtml = estimateWriter.toString();
			renderer.setDocumentFromString(estimateHtml);
			renderer.layout();
			renderer.writeNextDocument();
			renderer.finishPDF();
			byteArray = fos.toByteArray();
			estimate.setContent(new SerialBlob(byteArray));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
