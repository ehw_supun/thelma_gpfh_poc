package com.thelmagpfh.service.patient;

import com.thelmagpfh.model.entity.Estimate;

public interface DocumentService {

	void generateEstimatePdf(Estimate estimate);
	
}
