package com.thelmagpfh.persistence.dao;

import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.EmailTemplate;

public interface EmailTemplateDao extends Repository<EmailTemplate, Integer> {

	EmailTemplate findByName(String string);

}
