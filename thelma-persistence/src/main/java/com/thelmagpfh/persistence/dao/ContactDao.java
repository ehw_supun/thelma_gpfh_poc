package com.thelmagpfh.persistence.dao;

import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.Contact;

public interface ContactDao  extends Repository<Contact, Integer>{

	Contact save(Contact contact);
	
}
