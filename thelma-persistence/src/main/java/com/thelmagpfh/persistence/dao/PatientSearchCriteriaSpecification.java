package com.thelmagpfh.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.thelmagpfh.model.dto.PatientSearchCriteria;
import com.thelmagpfh.model.entity.Patient;
import com.thelmagpfh.util.CoreUtil;

public class PatientSearchCriteriaSpecification<T> implements Specification<Patient> {

	private PatientSearchCriteria patientSearchCriteria;

	public PatientSearchCriteriaSpecification(PatientSearchCriteria patientSearchCriteria) {
		this.patientSearchCriteria = patientSearchCriteria;
	}

	@Override
	public Predicate toPredicate(Root<Patient> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
		final List<Predicate> predicates = new ArrayList<Predicate>();
		if (!CoreUtil.isNull(patientSearchCriteria.getFirstName())) {
			predicates.add(builder.like(root.get("firstName"), patientSearchCriteria.getFirstName()+ "%"));
		}
		if (!CoreUtil.isNull(patientSearchCriteria.getLastName())) {
			predicates.add(builder.like(root.get("lastName"), patientSearchCriteria.getLastName()+ "%"));
		}
		if (!CoreUtil.isNull(patientSearchCriteria.getMrn())) {
			predicates.add(builder.like(root.get("mrn"), patientSearchCriteria.getMrn()+ "%"));
		}
		return builder.and(predicates.toArray(new Predicate[predicates.size()]));
	}

}
