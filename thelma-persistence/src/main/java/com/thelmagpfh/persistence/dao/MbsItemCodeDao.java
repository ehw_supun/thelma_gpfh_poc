package com.thelmagpfh.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.thelmagpfh.model.entity.MbsItemCode;

public interface MbsItemCodeDao extends Repository<MbsItemCode, String> {

	@Query("select c from MbsItemCode c where c.id like :q% or c.description like :q%")
	List<MbsItemCode> findMbsItemCodeByIdorDesc(@Param("q")String q);
	
}
