package com.thelmagpfh.persistence.dao;

import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.ServiceCode;

public interface ServiceCodeDao extends Repository<ServiceCode, String>{

	ServiceCode findById(String code);
}
