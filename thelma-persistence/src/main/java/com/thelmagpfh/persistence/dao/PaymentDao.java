package com.thelmagpfh.persistence.dao;

import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.Payment;

public interface PaymentDao extends Repository<Payment, Integer> {
	
	Payment save(Payment payment);

}
