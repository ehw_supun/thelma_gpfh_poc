package com.thelmagpfh.persistence.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.Patient;

public interface PatientDao extends Repository<Patient, Integer> {

	Patient save(Patient patient);
	
	Patient findById(Integer id);

	Page<Patient> findAll(Specification<Patient> spec, Pageable pageable);
}
