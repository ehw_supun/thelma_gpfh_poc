package com.thelmagpfh.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.Estimate;
import com.thelmagpfh.model.entity.Patient;

public interface EstimateDao extends Repository<Estimate, Integer>{

	Estimate save(Estimate estimate);
	
	Estimate findById(Integer id);
	
	List<Estimate> findByPatientIn(List<Patient> patients);
	
	@Query("select e from Estimate e join e.patient p where p.id=?1")
	List<Estimate> findByPatientId(Integer patientId);
	
}
