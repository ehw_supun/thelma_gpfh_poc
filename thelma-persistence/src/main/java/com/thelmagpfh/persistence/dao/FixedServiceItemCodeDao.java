package com.thelmagpfh.persistence.dao;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.FixedServiceItemCode;

public interface FixedServiceItemCodeDao extends Repository<FixedServiceItemCode, String> {

	List<FixedServiceItemCode> findAll();
}
