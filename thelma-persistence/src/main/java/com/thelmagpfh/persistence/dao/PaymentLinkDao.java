package com.thelmagpfh.persistence.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.thelmagpfh.model.entity.Payment;
import com.thelmagpfh.model.entity.PaymentLink;
import com.thelmagpfh.model.entity.PaymentStatus;

public interface PaymentLinkDao extends Repository<PaymentLink, Integer> {

	PaymentLink save(PaymentLink paymentLink);
	
	PaymentLink findByPaymentId(Integer paymentId);
	
	@Query("select sum(p.amount) from PaymentLink pl join pl.invoice i join pl.payment p where i.id=?1 and p.status=?2")
	BigDecimal totalPaymentAmount(Integer invoiceId, PaymentStatus paymentStatus);

	@Query("select p from PaymentLink pl join pl.invoice i join pl.payment p where i.id=?1 and p.status=?2")
	List<Payment> findCompletedPaymentsByInvoiceId(int invoiceId, PaymentStatus status);
}
