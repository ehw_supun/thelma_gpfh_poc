package com.thelmagpfh.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.thelmagpfh.model.entity.ProsthesisItemCode;

public interface ProsthesisItemCodeDao  extends Repository<ProsthesisItemCode, String> {

	@Query("select c from ProsthesisItemCode c where c.id like :q% or c.description like :q%")
	List<ProsthesisItemCode> findProsthesisItemCodeByIdorDesc(@Param("q")String q);
}
