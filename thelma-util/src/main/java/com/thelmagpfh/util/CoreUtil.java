package com.thelmagpfh.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

public class CoreUtil {


    private static final BigDecimal GST_PERCENTAGE = new BigDecimal(0.1);

    private static final String email_regex = "^(.+)@(.+)$";

    public static final String DEFAULT_DATE_FORMAT = new String("dd/MM/yyyy");

    public static final String DATE_FORMAT_PROMOTION_VALIDITY = new String("yyyy/MM/dd");

    public static final String DECIMAL_DOLLAR_FORMATTER = new String("#.00");

    public static final String MONEY_NUMBERFORMAT_REGEX = "###0.00";

    private static final String MONEY_FORMAT = new String(MONEY_NUMBERFORMAT_REGEX);

    private static final int REF_NUM_LENGTH = 4;

    private static final String REF_NUM_PADDING = "0000";

    private static final String DATE_FORMAT_DD_M_MYYYY = "ddMMyyyy";

    public static boolean isNull(Object object) {
        if (object instanceof String) {
            return ((String) object).length() == 0;
        } else if (object instanceof Object[]) {
            return ((Object[]) object).length == 0;
        } else if (object instanceof Collection) {
            return ((Collection<?>) object).isEmpty();
        } else if (object instanceof Map) {
            return ((Map<?, ?>) object).isEmpty();
        } else {
            return object == null;
        }
    }

    public static String buildSQLLike(String value) {
        if (value != null) {
            return "%".concat(value).concat("%");
        }
        return null;
    }

    public static String extractCityFromCityCode(String cityCode) {
        if (!CoreUtil.isNull(cityCode)) {
            if ((cityCode.indexOf('(') != -1)) {
                return cityCode.split("\\(")[0].trim();
            } else {
                return cityCode.trim();
            }
        }
        return null;
    }

    public static Date parseDate(String dateStr) {
        try {
            return new SimpleDateFormat(DEFAULT_DATE_FORMAT).parse(dateStr);
        } catch (ParseException e) {
            return null;
        } catch (NullPointerException e) {
            return null;
        }
    }

    public static Date parseDate(String dateStr, String format) {
        try {
            return new SimpleDateFormat(format).parse(dateStr);
        } catch (ParseException e) {
            return null;
        } catch (NullPointerException e) {
            return null;
        }
    }

    public static String formatDate(String dateFormat, Date date) {
        return date != null ? new SimpleDateFormat(dateFormat).format(date) : null;
    }

    public static BufferedImage generateBufferedImage(byte[] binaryData, boolean applyWatermark) throws IOException {
        BufferedImage old = ImageIO.read(new ByteArrayInputStream(binaryData));
        if (applyWatermark) {
            // logo-main2.png 237 x 70 px
            BufferedImage logoImage = ImageIO
                    .read(CoreUtil.class.getClassLoader().getResourceAsStream("logo-main2.png"));
            int targetWidth = old.getWidth() / 5;
            int targetHeight = targetWidth * logoImage.getHeight() / logoImage.getWidth();

            Image scaledTmpImage = logoImage.getScaledInstance(targetWidth, targetHeight, Image.SCALE_SMOOTH);

            Graphics2D g2d = old.createGraphics();
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.77f));
            g2d.drawImage(scaledTmpImage, old.getWidth() - targetWidth, old.getHeight() - targetHeight, null);
            g2d.dispose();
        }
        return old;
    }

    public static String codeClassToDao(Class<?> codeClass) {
        return codeClass.getSimpleName().substring(0, 1).toLowerCase()
                + codeClass.getSimpleName().substring(1, codeClass.getSimpleName().length()) + "Dao";
    }

    public static Date getEndOfTodaysTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);

        return calendar.getTime();
    }

    public static String getFormattedDollerValue(double value) {
        return new DecimalFormat(DECIMAL_DOLLAR_FORMATTER).format(value);
    }

    public static boolean isValidEmail(String email) {
        if (!isNull(email)) {
            Pattern pattern = Pattern.compile(email_regex);
            return pattern.matcher(email).matches();
        }
        return false;
    }

    public static String promotionSeoLink(String heading) {
        if (!isNull(heading)) {
            String seoUrl = heading.trim().replace(" ", "-");
            if (seoUrl.contains("--")) {
                // No need --
                seoUrl = seoUrl.replace("--", "-");
            }
            return seoUrl;
        }
        return null;
    }

    public static String formatRefNumber(String prefix, long refNumber) {
        String text = REF_NUM_PADDING + refNumber;
        text = text.substring(text.length() - REF_NUM_LENGTH, text.length());
        return (prefix + text);
    }

    public static int getDaysDifference(Date start, Date end) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(start);
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(end);

        return endCal.get(Calendar.DAY_OF_YEAR) - startCal.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * Returns the given BigDecimal formatted as a String using the Application
     * MONEY_FORMAT.
     * 
     * @param money
     *            BigDecimal.
     * @return String.
     */
    public static String getFormattedMoney(BigDecimal money) {
        return new DecimalFormat(MONEY_FORMAT).format((isSafeDecimal(money)) ? money : BigDecimal.ZERO);
    }

    /**
     * Checks whether the given Big Decimal is safe, i.e. is not null and is a
     * number.
     * 
     * @param number
     *            BigDecimal.
     * @return boolean.
     */
    private static boolean isSafeDecimal(BigDecimal number) {
        return (number != null && isSafeDouble(number.doubleValue()));
    }

    /**
     * Checks whether the given double is a number.
     * 
     * @param number
     *            double.
     * @return boolean.
     */
    private static boolean isSafeDouble(double number) {
        return (!Double.isNaN(number) && !Double.isInfinite(number));
    }

    public static BigDecimal calculateGst(BigDecimal totalIncGst) {
        return totalIncGst.multiply(GST_PERCENTAGE);
    }

    public static boolean isFutureDate(Date date) {
        if (date == null) {
            return false;
        }
        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return !df.format(today).equals(df.format(date)) && date.after(today);
    }

    public static boolean isTodayOrPastDate(Date date) {
        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return (date != null && (date.before(today) || df.format(date).equals(df.format(today))));
    }

    public static boolean isTodayOrFutureDate(Date date) {
        Date today = new Date();
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return (date != null && (date.after(today) || df.format(date).equals(df.format(today))));
    }

    public static boolean isSameOrLaterDate(Date thisDate, Date thatDate) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return thisDate != null && thatDate != null
                && (thisDate.after(thatDate) || df.format(thisDate).equals(df.format(thatDate)));
    }

    public static boolean isSameOrLaterDateNotInPast(Date thisDate, Date thatDate) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return thisDate != null && thatDate != null && isTodayOrFutureDate(thisDate)
                && ((thisDate.after(thatDate) || df.format(thisDate).equals(df.format(thatDate))));
    }

    public static boolean isLaterDateNotInPast(Date thisDate, Date thatDate) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return thisDate != null && thatDate != null && isFutureDate(thisDate)
                && ((thisDate.after(thatDate) || df.format(thisDate).equals(df.format(thatDate))));
    }

    public static boolean isSameOrEarlierDate(Date thisDate, Date thatDate) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_DD_M_MYYYY);
        return thisDate != null && thatDate != null
                && (thisDate.before(thatDate) || df.format(thisDate).equals(df.format(thatDate)));
    }

    public static Date addDaysToToday(int days) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static Date addDaysToToday(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static boolean booleanValue(Object pool) {
        if (pool == null) {
            return false;
        }
        if (pool instanceof Boolean) {
            return (Boolean) pool;
        }

        if (pool instanceof String) {
            return Boolean.valueOf((String) pool);
        }
        return false;
    }

}
